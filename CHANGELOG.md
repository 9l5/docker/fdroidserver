# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [3.0.0](../../compare/2.0.0...3.0.0) (2024-12-25)

## [2.0.0](../../compare/1.1.9...2.0.0) (2024-12-25)


### Features

* shrink down image by multistage build and selective include of runtime parts ([76a4984](76a498479cad99ae7e26fc6a2166e1ab1df26e2f))

### [1.1.9](../../compare/1.1.8...1.1.9) (2024-12-01)

### [1.1.8](../../compare/1.1.7...1.1.8) (2024-11-01)

### [1.1.7](../../compare/1.1.6...1.1.7) (2024-10-01)

### [1.1.6](../../compare/1.1.5...1.1.6) (2024-09-01)

### [1.1.5](../../compare/1.1.4...1.1.5) (2024-08-01)

### [1.1.4](../../compare/1.1.3...1.1.4) (2024-07-01)

### [1.1.3](../../compare/1.1.2...1.1.3) (2024-06-01)

### [1.1.2](../../compare/1.1.1...1.1.2) (2024-05-01)

## [2.0.0](../../compare/1.1.1...2.0.0) (2024-12-25)


### Features

* shrink down image by multistage build and selective include of runtime parts ([f473da5](f473da5687979a275aa286611506f0a6afc2d89b))

## [1.1.1](../../compare/1.1.0...1.1.1) (2024-04-17)


### Bug Fixes

* **androguard:** don't crash on res0/1 not be zero ([53f6288](53f62887541496a0f8519737d3359f927c971917))

## [1.1.0](../../compare/1.0.18...1.1.0) (2024-04-16)


### Features

* update base to bookworm, java to 17 ([230eb22](230eb220eb5d2e434bceb3f79d7d439fb185ed12))

### [1.0.18](../../compare/1.0.17...1.0.18) (2024-04-01)

### [1.0.17](../../compare/1.0.16...1.0.17) (2024-03-01)

## [1.0.16](../../compare/1.0.15...1.0.16) (2024-02-10)


### Bug Fixes

* pin versions, to circumvent non-working as of new major version of androguard ([fa0d5b8](fa0d5b8e52db65d1d78e167fce23e3810f48976f))

### [1.0.15](../../compare/1.0.14...1.0.15) (2024-02-01)

### [1.0.14](../../compare/1.0.13...1.0.14) (2024-01-01)

### [1.0.13](../../compare/1.0.12...1.0.13) (2023-12-01)

### [1.0.12](../../compare/1.0.11...1.0.12) (2023-10-01)

### [1.0.11](../../compare/1.0.10...1.0.11) (2023-09-01)

### [1.0.10](../../compare/1.0.9...1.0.10) (2023-08-01)

## [1.0.9](../../compare/1.0.8...1.0.9) (2023-07-01)


### Bug Fixes

* change base image to nginx:bullseye ([2018ad7](2018ad7eb64f59e011e368c3d47fe8c559e1400e))

### [1.0.8](../../compare/1.0.7...1.0.8) (2023-07-01)

### [1.0.7](../../compare/1.0.6...1.0.7) (2023-06-01)

### [1.0.6](../../compare/1.0.5...1.0.6) (2023-05-01)

### [1.0.5](../../compare/1.0.4...1.0.5) (2023-04-01)

### [1.0.4](../../compare/1.0.3...1.0.4) (2023-03-01)

### [1.0.3](../../compare/1.0.2...1.0.3) (2023-02-01)

### [1.0.2](../../compare/1.0.1...1.0.2) (2023-01-01)

### [1.0.1](../../compare/1.0.0...1.0.1) (2022-12-16)


### Bug Fixes

* add openjdk-11-jdk-headless to package list ([5cca9eb](5cca9ebc1b6d860f42e3e6d9c366e880fc231c2f))

## 1.0.0 (2022-12-16)
