#!/bin/bash
#

set -e

JAVA_INSTALLFOLDER="/usr/local/openjdk-25"

# jmods

mv $JAVA_INSTALLFOLDER/jmods jmods
mkdir $JAVA_INSTALLFOLDER/jmods

PACKAGES=(
    # seems like the thing I want to do, doesn't need _any_ jmod?
)

for i in "${!PACKAGES[@]}"; do
    mv jmods/${PACKAGES[$i]} $JAVA_INSTALLFOLDER/jmods/${PACKAGES[$i]}
done

# libs

mv $JAVA_INSTALLFOLDER/lib lib
mkdir $JAVA_INSTALLFOLDER/lib

PACKAGES=(
    "jvm.cfg"
    "libjava.so"
    "libjimage.so"
    "libjli.so"
    "libnet.so"
    "libnio.so"
    "libzip.so"
    "modules"
    "server"
    "tzdb.dat"
)

for i in "${!PACKAGES[@]}"; do
    mv lib/${PACKAGES[$i]} $JAVA_INSTALLFOLDER/lib/${PACKAGES[$i]}
done
